//khai báo mongoose
const mongoose = require("mongoose");

// Khai báo class Schema 
const Schema = mongoose.Schema;

// Khởi tạo 1 instance productSchema từ Class Schema
const productSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    stock: {
        type: Number,
        required: true
    },
    isPopular: {
        type: String,
        enum: ["false", "true"],
        default: "false"
    },
    isStatus: {
        type: String,
        enum: ["false", "true"],
        default: "false"
    },
},
{
    timestamps: true
});

module.exports = mongoose.model("Product", productSchema);