//khai báo mongoose
const mongoose = require("mongoose");

// Khai báo class Schema 
const Schema = mongoose.Schema;

// Khởi tạo 1 instance orderDetailSchema từ Class Schema
const orderDetailSchema = new Schema({
    order: {
        type: mongoose.Types.ObjectId,
        ref: "Order",
        required: true
    },
    product: {
        type: mongoose.Types.ObjectId,
        ref: "Product",
        required: true
    },
    quantity: {
        type: Number,
        required: true
    },
    price: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model("OrderDetail", orderDetailSchema);