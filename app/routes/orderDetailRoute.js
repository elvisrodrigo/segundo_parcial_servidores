//khai bao thu vien express
const express = require("express")

// Import orderDetail controller
const orderDetailController = require("../controllers/orderDetailController");

//Tạo ra router
const orderDetailRouter = express.Router();


orderDetailRouter.post('/orderDetails', orderDetailController.createOrderDetail);
orderDetailRouter.get('/orderDetails', orderDetailController.getAllOrderDetail);
orderDetailRouter.get('/orderDetails/:id', orderDetailController.getOrderDetailById);
orderDetailRouter.put('/orderDetails/:id', orderDetailController.updateOrderDetailById);
orderDetailRouter.delete('/orderDetails/:id', orderDetailController.deleteOrderDetailById);
 
module.exports = { orderDetailRouter }