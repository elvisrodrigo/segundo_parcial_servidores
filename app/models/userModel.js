//khai báo mongoose
const mongoose = require("mongoose");

// Khai báo class Schema 
const Schema = mongoose.Schema;

// Khởi tạo 1 instance userSchema từ Class Schema
const userSchema = new Schema({
    fullName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    address: {
        type: String,
        required: true
    },
    role: {
        type: String,
        enum: ["Customer", "Admin"],
        default: "Customer"
    },
    orders: [{
        type: mongoose.Types.ObjectId,
        ref: "Order"
    }],
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model("User", userSchema);