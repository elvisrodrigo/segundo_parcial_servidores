const Product = require('../models/productModel');

const createProduct = async (req, res) => {
  try {
    const { name, description, price, image, stock,isPopular,isStatus } = req.body;

    // Validate input fields
    if (!name || !description || !price || !image || !stock || !isPopular || !isStatus) {
      return res.status(400).json({
        status: "Error 400: Bad Request",
        message: "All fields are required"
      });
    }

    // Create new product instance
    const newProduct = new Product({
      name,
      description,
      price,
      image,
      stock,
      isPopular,
      isStatus
    });

    // Save product to the database
    await newProduct.save();

    return res.status(201).json({
      status: "Success 201: Created",
      message: "Product created successfully"
    });
  } catch (err) {
    return res.status(500).json({
      status: "Error 500: Internal Server Error",
      message: err.message
    });
  }
};


const getAllProduct = async (request, response) => {
    try {
        const products = await Product.find();
        return response.status(200).json({
            status: "success",
            data: products
        });
    } catch (error) {
        return response.status(500).json({
            status: "error",
            message: error.message
        });
    }
};

const getProductById = async (req, res) => {
    try {
      const productId = req.params.id;
      const product = await Product.findById(productId);
  
      if (!product) {
        return res.status(404).json({
          message: 'Product not found',
        });
      }
  
      return res.status(200).json(product);
    } catch (error) {
      return res.status(500).json({
        message: 'Server error',
        error: error.message,
      });
    }
  };

  const updateProductById = async (req, res) => {
    try {
      const productId = req.params.id;
      const {name, description, price, image, stock ,isPopular,isStatus } = req.body;
  
      // Check if user with given ID exists
      const product = await Product.findById(productId);
      if (!product) {
        return res.status(404).json({ message: "Product not found" });
      }
  
      // Update user's properties
      product.name = name;
      product.description = description;
      product.price = price;
      product.image = image;
      product.stock = stock;
      product.isPopular = isPopular;
      product.isStatus = isStatus;
      // Save updated user to database
      const updatedProduct = await product.save();
  
      res.status(200).json(updatedProduct);
    } catch (error) {
      res.status(400).json({ message: error.message });
    }


  
  };

  const deleteProduct = async (req, res) => {
    const { id } = req.params;
  
    try {
      const product = await Product.findByIdAndDelete(id);
  
      if (!product) {
        return res.status(404).json({
          status: "Error 404: Not Found",
          message: "Product not found",
        });
      }
  
      return res.status(200).json({
        status: "Delete successfully",
        message: "Product deleted successfully",
        data: product,
      });
    } catch (error) {
      return res.status(500).json({
        status: "Error 500: Internal Server Error",
        message: error.message,
      });
    }
  };

module.exports = {
    createProduct,
    getAllProduct,
    getProductById,
    updateProductById,
    deleteProduct

}
