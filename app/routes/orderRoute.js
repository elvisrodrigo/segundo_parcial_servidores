//khai bao thu vien express
const express = require("express")

// Import order controller
const orderController = require("../controllers/orderController");

//Tạo ra router
const orderRouter = express.Router();


orderRouter.post('/orders', orderController.createOrder);
orderRouter.get('/orders', orderController.getAllOrder);
orderRouter.get('/orders/:id', orderController.getOrderById);
orderRouter.put('/orders/:id', orderController.updateOrderById);
orderRouter.delete('/orders/:id', orderController.deleteOrderById);

module.exports = { orderRouter }