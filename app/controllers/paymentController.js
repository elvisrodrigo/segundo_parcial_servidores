//import model
const payMentModel = require("../models/paymentModel");

const createPayment = async (req, res) => {
    try {
        const { customer, method, amount } = req.body;

        // Tạo một instance payment mới từ model Payment
        const payment = new payMentModel({
            customer,
            method,
            amount
        });

        // Lưu payment vào database
        const savedPayment = await payment.save();

        res.status(201).json(savedPayment);
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
}

const getAllPayment = async (req, res) => {
    try {
        const payments = await payMentModel.find();
        res.json(payments);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}

const getPaymentById = async (req, res) => {
    const { id } = req.params;
    try {
        const payment = await payMentModel.findById(id);
        if (!payment) {
            return res.status(404).json({ message: "Payment not found" });
        }
        res.status(200).json(payment);
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
};

const updatePaymentById = async (req, res) => {
    try {
        const paymentId = req.params.id;
        const { method, status, amount } = req.body;

        const payment = await payMentModel.findById(paymentId);

        if (!payment) {
            return res.status(404).json({ message: "Payment not found" });
        }

        payment.method = method;
        payment.status = status;
        payment.amount = amount;
        payment.updatedAt = Date.now();

        const updatedPayment = await payment.save();
        res.json(updatedPayment);
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
};

const deletePaymentById = async (req, res) => {
    try {
        const { id } = req.params;
        const deletedPayment = await payMentModel.findByIdAndDelete(id);
        if (!deletedPayment) {
            return res.status(404).json({ message: "Payment not found" });
        }
        res.json({ message: "Payment deleted successfully" });
    } catch (error) {
        res.status(500).json({ message: error.message });
    }
}


module.exports = {
    createPayment,
    getAllPayment,
    getPaymentById,
    updatePaymentById,
    deletePaymentById,
}