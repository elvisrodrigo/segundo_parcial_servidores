//khai bao thu vien express
const express = require("express")

// Import promotion controller
const promotionController = require("../controllers/promotionController");

//Tạo ra router
const promotionRouter = express.Router();

promotionRouter.post('/promotions', promotionController.createPromotion);
promotionRouter.get('/promotions', promotionController.getAllPromotions);
promotionRouter.get('/promotions/:id', promotionController.getPromotionById);
promotionRouter.put('/promotions/:id', promotionController.updatePromotionById);
promotionRouter.delete('/promotions/:id', promotionController.deletePromotionById);

module.exports = { promotionRouter }