//khai bao thu vien express
const express = require("express")

// Import payment controller
const paymentController = require("../controllers/paymentController");

//Tạo ra router
const paymentRouter = express.Router();

paymentRouter.post('/payments', paymentController.createPayment);
paymentRouter.get('/payments', paymentController.getAllPayment);
paymentRouter.get('/payments/:id', paymentController.getPaymentById);
paymentRouter.put('/payments/:id', paymentController.updatePaymentById);
paymentRouter.delete('/payments/:id', paymentController.deletePaymentById);

module.exports = { paymentRouter }